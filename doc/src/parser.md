# Parser.fnl
Function signature:

```
(parser file config)
```

Accepts `file` as path to some Fennel module, and `config` table.
Generates module documentation and writes it to `file` with `.md`
extension, creating it if not exists.



<!-- Generated with Fenneldoc 0.0.5
     https://gitlab.com/andreyorst/fenneldoc -->
